const express = require('express');
const cors = require('cors');
const path = require('path');
const PORT = process.env.PORT || 3000;
const corsOptions = {
  origin: '*'
};

const app = express();
const db = require('./queries');
const { request } = require('http');
const { response } = require('express');
const avatarholder = require('avatarholder');
const { v4: uuidv4 } = require('uuid');


app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (request, response) => {
  response.json({ info: 'STAPP API' });
});

app.get('/get-avatar', (request, response) => {
  const name = request.body.name;
  const surname = request.body.surname;
  const avatar = avatarholder.generateAvatarToFile(name + surname, 
    'http://localhost:' + PORT + '/files/avatars/' + uuidv4() + '.jpg',

    );
    console.log(avatar);
    response.json({avatarPath:avatar});
})

app.get('/get-patients', db.getPatients);
app.post('/add-patient', db.createPatient);
app.delete('/delete-patient', db.deletePatient);
app.put('/update-patient', db.updatePatient);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}...`);
});

app.use('/files', express.static(path.join(__dirname, 'assets/files')));