module.exports = {
  deleteLast : (chr, str) => {
    let finalString = '';
    let indexPosition = str.lastIndexOf(chr);
    finalString = str.substring(0, indexPosition) +
      str.substring(indexPosition+1, str.length);
    return finalString;
  }
}