const Pool = require('pg').Pool;
const pool = new Pool ({
  user: 'zdqesitemtppjs',
  host: 'ec2-174-129-37-144.compute-1.amazonaws.com',
  database: 'd5ilhrgnqshmo6',
  password: 'e25eff958ca9c23eb9d2b93a3631a1f47034b44a29f976194caded267a96c09b',
  port: '5432',
  ssl: {
    rejectUnauthorized: false
  }
});
const utils = require('./utils');

const getPatients = (request, response) => {
  pool.query('SELECT * FROM patients ORDER BY id ASC', (error, result) => {
    if(error)
      return response.status(406).send(error);
    return response.status(200).json(result.rows);
  });
}

const createPatient = (request, response) => {
  const { name, surname, birth } = request.body;
  const test0 = request.body.test0 ? request.body.test0 : 0;
  const test1 = request.body.test1 ? request.body.test1 : 0;
  console.log(request.body);
  if(!name || !surname || !birth)
    return response.status(400).send('name, surname, birth field is required.');
    

  pool.query('INSERT INTO patients (name, surname, birth, test0, test1) VALUES ($1, $2, $3, $4, $5)', 
    [name, surname, birth, test0, test1], (error, result) => {
      if(error)
        return response.status(406).send('Patient cannot added.');

      return response.status(201).send('Patient succesfully added.');
    });
}

const deletePatient = (request, response) => {
  const id = request.query.id;
  if(!id)
    return response.status(400).send('Invalid patient ID');
  pool.query('DELETE FROM patients WHERE id=$1', [id], (error, result) => {
    if(error)
      return response.status(406).send('Patient cannot removed.');
    return response.status(200).send('Patient succesfully removed.');
  })
}

const updatePatient = (request, response) => {
  const id = request.query.id;
  if(!id)
    return response.status(400).send('Invalid patient ID');
  let queryStr = 'UPDATE patients SET';
  let fieldNO = 1;
  let valueList = [];
  const name = request.body.name;
  const surname = request.body.surname;
  const birth = request.body.birth;
  const test0 = request.body.test0;
  const test1 = request.body.test1;
  if(name){
    queryStr += ' name = $' + fieldNO++ + ',';
    valueList.push(name);
  }
    
  if(surname){
    queryStr += ' surname = $' + fieldNO++ + ',';
    valueList.push(surname);
  }
    
  if(birth){
    queryStr += ' birth = $' + fieldNO++ + ',';
    valueList.push(birth);
  }
    
  if(test0){
    queryStr += ' test0 = $' + fieldNO++ + ',';
    valueList.push(test0);
  }
    
  if(test1){
    queryStr += ' test1 = $' + fieldNO++ + ',';
    valueList.push(test1);
  }
    
  queryStr += ' WHERE id = $' + fieldNO++;
  valueList.push(id);

  console.log(`Query Str is : ${utils.deleteLast(',', queryStr)}\nQuery Params is : ${valueList}`);
  pool.query(utils.deleteLast(',', queryStr), valueList, (error, result) => {
    if(error)
      return response.status(406).send('Patient cannot updated. ');
    return response.status(200).send('Patient update succesfully');
  })
}

module.exports = {
  getPatients,
  createPatient,
  deletePatient,
  updatePatient
}